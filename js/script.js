$('.carousel').carousel({
	interval: 4000
})

function showHide(element_id) {

	if (document.getElementById(element_id)) { 
		var obj = document.getElementById(element_id); 
		if (obj.style.display != "block") { 
			obj.style.display = "block";
			}
		else obj.style.display = "none"; 
		}
	else alert("Элемент с id: " + element_id + " не найден!");
}

function naxElement (element_id) {
	if (document.getElementById(element_id)) {      
		var obj = document.getElementById(element_id); 
		if (obj.style.display == "block") { 
			obj.style.display = "none"; 
			}
		else obj.style.display = "block";
		}
	else alert("Элемент с id: " + element_id + " не найден!");	
}

$(document).ready(function() {
	$("#quote-carousel").swiperight(function() {
		$(this).carousel('prev');
	});
	$("#quote-carousel").swipeleft(function() {
		$(this).carousel('next');
	});
});

$('.carousel').bcSwipe({ threshold: 100 }); // touch sesivity


